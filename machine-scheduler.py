import datetime
import time
import threading
import random


################################################################################
#   Handle all connections and rights for the server
################################################################################
class my_task():
    name = None
    priority = -1
    period = -1
    execution_time = -1
    last_execution_time = None
    last_execution_time_p1 = 0
    last_execution_time_p2 = 0
    last_execution_time_m1 = 0
    last_execution_time_m2 = 0

    ############################################################################
    def __init__(self, name, priority, period, execution_time, last_execution):
        self.name = name
        self.priority = priority
        self.period = period
        self.execution_time = execution_time

    def sauvegarde(self):
        self.last_execution_time_p1 = self.execution_time
        self.last_execution_time_p2 = self.execution_time
        self.last_execution_time_m1 = self.execution_time
        self.last_execution_time_m2 = self.execution_time


        

    ############################################################################
    def run(self):
        global passed_time
        global tank
        global stock1
        global stock2


        # Update last_execution_time
        self.last_execution_time = datetime.datetime.now()


        if self.name == "Pump 1" and self.period % 5 != 0:
            return
        if self.name == "Pump 2" and self.period % 15 != 0:
            return
        if self.name == "Machine 1" and self.period % 5 != 0:
            return
        if self.name == "Machine 2" and self.period % 5 != 0:
            return

        print(self.name + " : Starting task (" + self.last_execution_time.strftime(
            "%H:%M:%S") + ") : execution time = " + str(self.execution_time))

        print(str(tank) + " " + str(stock1) + " " + str(stock2))

        if self.name == "Pump 1" and self.last_execution_time_p1 <= 0:
            self.execution_time = 2
        if self.name == "Pump 2" and self.last_execution_time_p1 <= 0:
            self.execution_time = 3
        if self.name == "Machine 1" and self.last_execution_time_m1 <= 0:
            self.execution_time = 5
        if self.name == "Machine 2" and self.last_execution_time_m2 <= 0:
            self.execution_time = 3


        while (1):

            if (self.name == "Pump 1" or self.name == "Pump 2") and tank == 50:
                print("Pump bloqued because tank is Full")
                return
            elif (self.name == "Pump 1" and tank + 10 > 50) or (self.name == "Pump 2" and tank + 20 > 50) :
                print("Pump bloqued because no more stockage space available")
                return

                
            if self.name == "Machine 1" and tank >= 25:
                if stock1 // 4 >= stock2:
                    print("Machine 1 bloqued because priority is given to motors")
                    return
                else:
                    stock1 += 1
                    
                
            if self.name == "Machine 2" and tank >= 5:
                if stock1 // 4 < stock2:
                    print("Machine 2 bloqued because priority is given to wheels")
                    return
                else:
                    stock2 += 1

            self.execution_time -= 1

            passed_time += 1
            
            time.sleep(1)

            if self.execution_time <= 0:
                if self.name == "Pump 1":
                    tank += 10
                    print("Pump 1 : Produce 10 oil")
                elif self.name == "Pump 2":
                    tank += 20
                    print("Pump 2 : Produce 20 Oil")
                elif self.name == "Machine 1":
                    tank -= 25
                    stock1 += 1
                    print("Machine 1 : Produce 1 motor")
                elif self.name == "Machine 2":
                    tank -= 5
                    stock2 += 1
                    print("Machine 2 : Produce 1 wheel")

                print(self.name + " : Terminating normally (" + datetime.datetime.now().strftime("%H:%M:%S") + ")")
                self.sauvegarde()
                return


            self.sauvegarde()


####################################################################################################
#
#
#
####################################################################################################
if __name__ == '__main__':

    passed_time = 0
    tank = 0
    stock1 = 0
    stock2 = 0



    last_execution = datetime.datetime.now()

    # Instanciation of task objects
    task_list = [
        my_task(name="Pump 1", priority=1, period=5, execution_time=2, last_execution=last_execution),
        my_task(name="Pump 2", priority=1, period=15, execution_time=3, last_execution=last_execution),
        my_task(name="Machine 1", priority=1, period=5, execution_time=5, last_execution=last_execution),
        my_task(name="Machine 2", priority=1, period=5, execution_time=3, last_execution=last_execution)
    ]

    # Global scheduling loop
    incrementation = 0
    while (passed_time < 120):
        print("\nScheduler tick " + str(incrementation) + " : " + datetime.datetime.now().strftime("%H:%M:%S"))
        incrementation += 1

        # Reinit watchdog
        #watchdog = False
        #my_watchdog.current_cpt = 10
        # results
        for task_to_run in task_list:
            print("The current time is: "+str(passed_time));
            print("the tanks contains : " +str(tank));
            print("number of wheels: "+str(stock1));
            print("number of motors: "+str(stock2));
            print()


            task_to_run.run()

    print(str(passed_time) + " " + str(stock1) + " " + str(stock2))
